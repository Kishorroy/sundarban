<?php

require_once ("../../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;

use App\Computer\Computer;

$obj = new Computer();
$obj->setData($_GET);

$oneData = $obj->delete();

Utility::redirect("index.php");