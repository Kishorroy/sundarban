<?php

require_once ("../../../../vendor/autoload.php");

use App\Message\Message;

use App\Computer\Computer;

$obj = new Computer();
$obj->setData($_GET);

$oneData = $obj->view();



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profile Picture</title>
    <link rel="stylesheet" href="../../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>

<h1> Computer & Laptop - Edit Form </h1>

<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center" >
        <?php

        echo Message::message();

        ?>
    </div>
</div>



<div class="container">
    <div class="row">
        <div class="col-md-4">
            <form action="update.php" method="POST" enctype="multipart/form-data">

                <div class="form-group">
                    <input type="hidden" class="form-control" value="<?php echo $oneData->id ?>" name="id" >
                </div>



                <div class="form-group">
                    <label for="Title">Title</label>
                    <input type="text" class="form-control" value="<?php echo $oneData->title ?>" name="Title" required="">
                </div>

                <div class="form-group">
                    <label for="Image">Image</label>
                    <input type="file" class="form-control" name="Image" required="">
                </div>

                <div class="form-group">
                    <label for="Content">Content</label>
                    <input type="text" class="form-control" value="<?php echo $oneData->content ?>"  name="Content" required=""></input>
                </div>
                <div class="form-group">
                    <label for="Price">Price</label>
                    <input type="text" class="form-control" value="<?php echo $oneData->price ?>" name="Price" required=""></input>
                </div>
                <div class="form-group">
                    <label for="Subcat">Sub Catagory</label>
                    <select name="Subcat">
                        <option>Select One</option>
                        <option value="24">Mac</option>
                        <option value="25">HP</option>
                        <option value="26">DELL</option>
                        <option value="27">SONY</option>
                        <option value="28">LENOVO</option>
                        <option value="29">Samsung</option>
                        <option value="30">Asus</option>
                        <option value="31">Toshiba</option>
                        <option value="32">Acer</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="Catagory">Catagory</label>
                    <select name="Catagory">
                        <option value="8">
                            Computer & Laptop
                        </option>
                    </select>
                </div>


                <button type="submit" class="btn btn-primary">Update</button>
                <br><br>
            </form>
        </div>
    </div>



</div>



<script src="../../.././resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script>


    $(function ($) {

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);
        $("#message").fadeOut(500);

    });


</script>


</body>
</html>
