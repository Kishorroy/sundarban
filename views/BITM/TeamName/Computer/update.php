<?php
require_once ("../../../../vendor/autoload.php");

$obj  = new \App\Computer\Computer();

$obj->setData($_POST);
$oneData = $obj->view();

$fileName = $oneData->image;

if( !empty($_FILES["Image"]["name"]) ){

    // Start of physically moving file to its destination
    $fileName =   time().$_FILES["Image"]["name"];

    $source = $_FILES["Image"]["tmp_name"];

    $destination = "Uploads/".$fileName;

    move_uploaded_file($source, $destination);

    // End of physically moving file to its destination


}

// Start of the process to store file name to the table
$_POST["Image"] = $fileName;

$obj->setData($_POST);

$obj->update();
\App\Utility\Utility::redirect("index.php");
// End of the process to store file name to the table
