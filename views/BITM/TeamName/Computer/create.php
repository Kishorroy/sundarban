<?php

require_once ("../../../../vendor/autoload.php");

use App\Message\Message;


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">


    <link rel="stylesheet" href="../../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">
    <script src="../../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
    <script src="../../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->



</head>
<body style="background-color: #8c8c8c" >

<div style="height: 50px;">
    <div id="message" class="btn-danger text-center">
        <?php
        echo Message::message();
        ?>
    </div>

<div class="navbar container">
    <a href='create.php' class='btn btn-lg bg-success'>Create</a>
    <a href='index.php' class='btn btn-lg bg-danger'>Active List</a>
</div>


<div class="container">


    <div class="col-sm-3"></div>
    <div class="col-lg-6" style="color: #2098d1; background:rgba(0,0,0,0.5); margin-top: 100px;margin-bottom: 150px; border-radius: 10px;padding-top: 10px;padding-bottom: 10px;font-family: 'Comic Sans MS'">


        <form action="store.php" method="post" enctype="multipart/form-data">
            <fieldset>
                <h2 style="text-align: center"><b> Computer & Laptop - Add Form </b></h2>
                <div class="container">
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-sm-5" style="padding-left: 75px">
                            <div class="form-group">
                                <label for="Title">Title</label>
                                <input type="text" class="form-control" placeholder="Title" name="Title" required="">
                            </div>
                            <div class="form-group">
                                <label for="Image">Image</label>
                                <input type="file" class="form-control" name="Image" required="">
                            </div>
                            <div class="form-group">
                                <label for="Content">Content</label>
                                <textarea class="form-control" placeholder="Content" name="Content" required=""></textarea>
                            </div>
                            <div class="form-group">
                                <label for="Price">Price</label>
                                <input type="text" class="form-control" placeholder="Price" name="Price" required=""></input>
                            </div>
                            <div class="form-group">
                                <label for="Subcat">Sub Catagory</label>
                                <select name="Subcat">
                                    <option>Select One</option>
                                    <option value="24">Mac</option>
                                    <option value="25">HP</option>
                                    <option value="26">DELL</option>
                                    <option value="27">SONY</option>
                                    <option value="28">LENOVO</option>
                                    <option value="29">Samsung</option>
                                    <option value="30">Asus</option>
                                    <option value="31">Toshiba</option>
                                    <option value="32">Acer</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="Catagory">Catagory</label>
                                <select name="Catagory">
                                <option value="8">
                                    Computer & Laptop
                                </option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form></div>

    <div class="col-sm-3"></div>


</div>



</div>



<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script>


    $(function ($) {

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);
        $("#message").fadeOut(500);

    });


</script>


</body>
</html>
