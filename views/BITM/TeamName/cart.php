<?php
include_once ('../../../vendor/autoload.php');
if(!isset($_SESSION)) session_start();
use App\Utility\Utility;
use App\Shopping\Shopping;
$obj=new Shopping();
$obj->setData($_GET);

$allData = $obj->details();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sundarban</title>
    <meta name="description" content="Examples for creative website header animations using Canvas and JavaScript" />
    <meta name="keywords" content="header, canvas, animated, creative, inspiration, javascript" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../../../favicon.ico">
    <link rel="stylesheet" type="text/css" href="../../../resources/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../../../resources/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="../../../resources/css/component.css" />
    <link href='http://fonts.googleapis.com/css?family=Raleway:200,400,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../../../resources/css/nav.css">
    <link rel="stylesheet" href="../../../resources/css/slide.css">

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js">


    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>

<h1 align="center">Shopping Cart</h1><hr>
    <div class="container" style="margin-top: 75px; margin-left: -100px">

        <div class="col-lg-offset-3" style="alignment: center">

            <?php
            foreach ($allData as $all){

                echo" <table class='table table-bordered table table-responsive'>
 
                        <tr style='background-color: whitesmoke'> <td style='font-family: Cambria Math; font-size: 20px'> Product</td> 
                        <td style='font-family: Cambria Math; font-size: 20px'>Unit Price </td> 
                        <td style='font-family: Cambria Math; font-size: 20px'> Quantity </td> 
                        <td style='font-family: Cambria Math; font-size: 20px'> Discount </td> 
                        <td style='font-family: Cambria Math; font-size: 20px'> Total</td>
                        <td style='font-family: Cambria Math; font-size: 20px'> </td> </tr>
                        
                       <tr>
                            <td style='font-family: Cambria Math; '>
                            
                                <div><img src='../../../resources/images/$all->image' style='height: 100px; width:100px'>  <b>$all->title</b></div>
                             </td>
                              
                            <td style='font-family: Cambria Math; font-size: 20px'> $all->price Tk.</td> 
                            
                            <td> <input type='number'> </td> 
                            
                            <td style='font-family: Cambria Math; font-size: 20px'> 5%</td> 
               
                            <td style='font-family: Cambria Math; font-size: 20px'> </td> 
                          
                            <td style='font-family: Cambria Math; font-size: 20px'> <a href='../homepage.php' class='btn btn-danger btn btn-sm'>Delete item</a></td> </tr>
                        
                             </td> 
                            
                            
                       </tr>
         
                       </table>  
                       
                       <a href='../homepage.php' class='btn btn-primary btn-lg'>Continue shopping</a>
                       <a href='bill.php' class='btn btn-success btn-lg' style='margin-left: 460px'>Proceed to Checkout</a>
                       
                       ";

            }?>




        </div>


    </div>


</body>
</html>