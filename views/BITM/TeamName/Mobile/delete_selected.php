<?php

require_once ("../../../../vendor/autoload.php");
use App\Mobile\Mobile;
use App\Utility\Utility;

$obj = new Mobile();

$selectedIDs =   $_POST["mark"];

$obj->deleteMultiple($selectedIDs);

$path = $_SERVER['HTTP_REFERER'];

Utility::redirect($path);