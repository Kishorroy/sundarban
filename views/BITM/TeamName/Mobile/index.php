<?php

require_once ("../../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\Utility\Utility;
use App\Mobile\Mobile;


$obj = new Mobile();

$allData  =  $obj->index();

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <link rel="stylesheet" href="../../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="../../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">
    <script src="../../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
    <script src="../../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

</head>
<body style="background-color: #2e6da4;">

<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center" >
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>





<div  class="container">


    <div>
        <a href='create.php' class='btn btn-lg bg-success'>Create</a>

        <a href="../Computer/create.php" class="btn btn-lg bg-info"><b>Computer & Laptop</b></a></button>
        <a href="#"  class="btn btn-lg bg-info"><b>Computer Accesories</b></a></button>
        <a href="#"  class="btn btn-lg bg-info"><b>Mobile Accesories</b></a></button>
        <a href="#" class="btn btn-lg bg-info"><b>Software</b></a></button>
        <button id="DeleteSelected" class='btn btn-lg bg-danger'>Delete Selected</button>







    </div>




    <div class="bg-info text-center"><h1>Mobile & Notepad - Active List</h1></div>

    <table border="1px" class="table table-bordered table-striped">

        <tr>
            <th>Select all  <input id='select_all' type='checkbox' value='select all'></th>
            <th> Serial </th>
            <th> ID </th>
            <th> Name </th>
            <th> Profile Pictures </th>
            <th> Content </th>
            <th> price </th>
            <th> Sub Category </th>
            <th> Category </th>
            <th> Action Buttons </th>

        </tr>
        <form  id="multiple" method="post">

            <?php


            $serial=1;

            foreach ($allData as $oneData){

                if($serial%2) $bgColor = "AQUA";
                else $bgColor = "#ffffff";

                echo "
    
                                  <tr  style='background-color: $bgColor'>
    
                                     <td style='padding-left: 4%'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
    
                                     <td style='width: 10%; text-align: center'>$serial</td>
                                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                                     <td style='width: 20%;'>$oneData->title</td>
                                     <td><img width='100px' height='100px' src='Uploads/$oneData->image'>  </a>  </td>
                                     <td style='width: 20%;'>$oneData->content</td>
                                     <td style='width: 20%;'>$oneData->price</td>
                                     <td style='width: 20%;'>$oneData->subcat</td>
                                     <td style='width: 20%;'>$oneData->category</td>
    
                                     <td>
                                       
                                       <a href='edit.php?id=$oneData->id' class='btn btn-primary'>Edit</a>
                                       
                                       <a href='delete.php?id=$oneData->id' onclick='return doConfirm()' class='btn btn-danger'>Delete</a>
                                       
    
                                     </td>
                                  </tr>
                              ";
                $serial++;

            }

            ?>

        </form>

    </table>


</div>




<script>


    $(document).ready(function () {

        $("#TrashSelected").click(function () {

            $("#multiple").attr("action","trash_selected.php");
            $("#multiple").submit();
        });

        $("#DeleteSelected").click(function () {

            $("#multiple").attr("action","delete_selected.php");
            $("#multiple").submit();
        });


    });




    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });




    function doConfirm() {
        return confirm("Are you sure you want to delete?");
    }


    $(function ($) {

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);
        $("#message").fadeOut(500);

    });


</script>

<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->


</body>
</html>