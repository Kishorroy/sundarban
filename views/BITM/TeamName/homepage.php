


;<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>Sundarban</title>
		<meta name="description" content="Examples for creative website header animations using Canvas and JavaScript" />
		<meta name="keywords" content="header, canvas, animated, creative, inspiration, javascript" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../../../../favicon.ico">
		<link rel="stylesheet" type="text/css" href="../../../resources/css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="../../../resources/css/demo.css" />
		<link rel="stylesheet" type="text/css" href="../../../resources/css/component.css" />
		<link href='http://fonts.googleapis.com/css?family=Raleway:200,400,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../../../resources/css/nav.css">
        <link rel="stylesheet" href="../../../resources/css/slide.css">


		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container demo-1">
			<div class="content">
				<div id="large-header" class="large-header">
					<canvas id="demo-canvas"></canvas>
					<h1 class="main-title">সুন্দরবন<br> <span class="thin" style="font-size: 25px;">কেনাকাটার সহজ উপায়</span></h1>
				</div>
<?php if(!isset($_GET['login'])){  ?>
                    <div class="codrops-top clearfix">
                        <a class="codrops-icon codrops-icon-prev" href="User/Profile/signin.php"><span>Login</span></a>
                        <a class="codrops-icon codrops-icon-drop" href="User/Profile/signup.php"><span>signup</span></a>
                    </div>
            <?php } else { ?>
                <div class="codrops-top clearfix">

                    <a class="codrops-icon codrops-icon-drop" href="homepage.php"><span>Logout</span></a>
                </div>
    <?php
}
?>
				<div class="codrops-header">
					<h1>Biggest market place of Bangladesh <span>Join the millions to buy products everyday in local communities around the Bangladesh</span></h1>
					<nav class="codrops-demos">
                        <div class="w3-dropdown-hover">
                            <button class="w3-button w3-red">Computer & Laptop &#8681;</button>
                            <div class="w3-dropdown-content w3-bar-block w3-border">
                                <a href="../TeamName/MAC/index.php" class="w3-bar-item w3-button">MAC</a>
                                <a href="../TeamName/HP/index.php" class="w3-bar-item w3-button">HP</a>
                                <a href="../TeamName/DELL/index.php" class="w3-bar-item w3-button">DELL</a>
                                <a href="#" class="w3-bar-item w3-button">SONY</a>
                                <a href="#" class="w3-bar-item w3-button">LENOVO</a>
                                <a href="#" class="w3-bar-item w3-button">SAMSUNG</a>
                                <a href="#" class="w3-bar-item w3-button">ASUS</a>
                                <a href="#" class="w3-bar-item w3-button">TOSHIBA</a>
                                <a href="#" class="w3-bar-item w3-button">ACER</a>
                            </div>
                        </div>
                        <div class="w3-dropdown-hover">
                            <button class="w3-button w3-blue">Mobile & Notepad &#8681;</button>
                            <div class="w3-dropdown-content w3-bar-block w3-border">
                                <a href="chk.php?value=34" class="w3-bar-item w3-button">Samsung</a>
                                <a href="chk.php?value=35" class="w3-bar-item w3-button">Xiaomi</a>
                                <a href="chk.php?value=33" class="w3-bar-item w3-button">Iphone</a>
                                <a href="chk.php?value=36" class="w3-bar-item w3-button">Oppo</a>
                                <a href="chk.php?value=37" class="w3-bar-item w3-button">Huawei</a>
                                <a href="chk.php?value=38" class="w3-bar-item w3-button">Walton</a>
                                <a href="chk.php?value=39" class="w3-bar-item w3-button">Lenovo</a>
                            </div>
                        </div>
                        <div class="w3-dropdown-hover">
                            <button class="w3-button w3-green">Camera & Lens &#8681;</button>
                            <div class="w3-dropdown-content w3-bar-block w3-border">
                                <a href="#" class="w3-bar-item w3-button">Nikon Camera Body</a>
                                <a href="#" class="w3-bar-item w3-button">Canon Camera Body</a>
                                <a href="#" class="w3-bar-item w3-button">Nikon Camera lens</a>
                                <a href="#" class="w3-bar-item w3-button">Canon Camera lens</a>
                                <a href="#" class="w3-bar-item w3-button">Digital Camera Body</a>
                                <a href="#" class="w3-bar-item w3-button">Fujiflim camera Body</a>
                            </div>
                        </div>
                        <div class="w3-dropdown-hover">
                            <button class="w3-button w3-purple">Television & Monitor &#8681;</button>
                            <div class="w3-dropdown-content w3-bar-block w3-border">
                                <a href="#" class="w3-bar-item w3-button">Sony</a>
                                <a href="#" class="w3-bar-item w3-button">LG</a>
                                <a href="#" class="w3-bar-item w3-button">Panasonic</a>
                                <a href="#" class="w3-bar-item w3-button">Walton</a>
                                <a href="#" class="w3-bar-item w3-button">Samsung</a>
                                <a href="#" class="w3-bar-item w3-button">Toshiba</a>
                            </div>
                        </div>
                        <div class="w3-dropdown-hover">
                            <button class="w3-button w3-indigo">Office Equipment &#8681;</button>
                            <div class="w3-dropdown-content w3-bar-block w3-border">
                                <a href="#" class="w3-bar-item w3-button">Phone set</a>
                                <a href="#" class="w3-bar-item w3-button">Projector </a>
                                <a href="#" class="w3-bar-item w3-button">CC camera</a>
                                <a href="#" class="w3-bar-item w3-button">IP camera</a>
                                <a href="#" class="w3-bar-item w3-button">Projector Screen </a>
                                <a href="#" class="w3-bar-item w3-button">Fax</a>
                                <a href="#" class="w3-bar-item w3-button">Photocopier</a>
                                <a href="#" class="w3-bar-item w3-button">Scanner</a>
                                <a href="#" class="w3-bar-item w3-button">Printer </a>
                                <a href="#" class="w3-bar-item w3-button">Barcode Scanner</a>

                            </div>
                        </div>
                        <br><br>
                        <div class="w3-dropdown-hover">
                            <button class="w3-button w3-pink">Network Accessories &#8681;</button>
                            <div class="w3-dropdown-content w3-bar-block w3-border">
                                <a href="#" class="w3-bar-item w3-button">Router</a>
                                <a href="#" class="w3-bar-item w3-button">Utp cable</a>
                                <a href="#" class="w3-bar-item w3-button">Switch</a>
                                <a href="#" class="w3-bar-item w3-button">Hub</a>
                                <a href="#" class="w3-bar-item w3-button">LAN card</a>
                                <a href="#" class="w3-bar-item w3-button">Edge Modem</a>
                            </div>
                        </div>
                        <div class="w3-dropdown-hover">
                            <button class="w3-button w3-orange">Computer Accessories &#8681;</button>
                            <div class="w3-dropdown-content w3-bar-block w3-border">
                                <a href="#" class="w3-bar-item w3-button">Processor</a>
                                <a href="#" class="w3-bar-item w3-button">Desktop ram</a>
                                <a href="#" class="w3-bar-item w3-button">Graphics card</a>
                                <a href="#" class="w3-bar-item w3-button">cooling fan</a>
                                <a href="#" class="w3-bar-item w3-button">keyboard</a>
                                <a href="#" class="w3-bar-item w3-button">mouse</a>
                                <a href="#" class="w3-bar-item w3-button">ups</a>
                                <a href="#" class="w3-bar-item w3-button">Mainboard</a>
                                <a href="#" class="w3-bar-item w3-button">Optical device</a>
                                <a href="#" class="w3-bar-item w3-button">External HDD</a>
                                <a href="#" class="w3-bar-item w3-button">pendrive</a>
                                <a href="#" class="w3-bar-item w3-button">Tv card</a>
                            </div>
                        </div>
                        <div class="w3-dropdown-hover">
                            <button class="w3-button w3-yellow">Mobile Accessories &#8681;</button>
                            <div class="w3-dropdown-content w3-bar-block w3-border">
                                <a href="#" class="w3-bar-item w3-button">Earphone</a>
                                <a href="#" class="w3-bar-item w3-button">Headphone</a>
                                <a href="#" class="w3-bar-item w3-button">charger</a>
                            </div>
                        </div>
                        <div class="w3-dropdown-hover">
                            <button class="w3-button w3-aqua">Software &#8681;</button>
                            <div class="w3-dropdown-content w3-bar-block w3-border">
                                <a href="../TeamName/Antivirus/index.php" class="w3-bar-item w3-button">Antivirus</a>
                                <a href="#" class="w3-bar-item w3-button">Accounting</a>
                                <a href="#" class="w3-bar-item w3-button">operating system</a>
                                <a href="#" class="w3-bar-item w3-button">Engineering design</a>
                                <a href="#" class="w3-bar-item w3-button">illustration</a>
                                <a href="#" class="w3-bar-item w3-button">Office software</a>
                            </div>
                        </div>

					</nav>
				</div>
			</div>
            <div class="slider" id="main-slider"><!-- outermost container element -->
                <div class="slider-wrapper"><!-- innermost wrapper element -->
                    <img src="../../../resources/images/slide.jpg" alt="First" class="slide" /><!-- slides -->
                    <img src="../../../resources/images/silde2.jpg" alt="Second" class="slide" />
                    <img src="../../../resources/images/silde3.jpg" alt="Third" class="slide" />
                </div>
            </div>
        </div>

            <script>
            (function() {

                function Slideshow( element ) {
                    this.el = document.querySelector( element );
                    this.init();
                }

                Slideshow.prototype = {
                    init: function() {
                        this.wrapper = this.el.querySelector( ".slider-wrapper" );
                        this.slides = this.el.querySelectorAll( ".slide" );
                        this.previous = this.el.querySelector( ".slider-previous" );
                        this.next = this.el.querySelector( ".slider-next" );
                        this.index = 0;
                        this.total = this.slides.length;
                        this.timer = null;

                        this.action();
                        this.stopStart();
                    },
                    _slideTo: function( slide ) {
                        var currentSlide = this.slides[slide];
                        currentSlide.style.opacity = 1;

                        for( var i = 0; i < this.slides.length; i++ ) {
                            var slide = this.slides[i];
                            if( slide !== currentSlide ) {
                                slide.style.opacity = 0;
                            }
                        }
                    },
                    action: function() {
                        var self = this;
                        self.timer = setInterval(function() {
                            self.index++;
                            if( self.index == self.slides.length ) {
                                self.index = 0;
                            }
                            self._slideTo( self.index );

                        }, 6000);
                    },
                    stopStart: function() {
                        var self = this;
                        self.el.addEventListener( "mouseover", function() {
                            clearInterval( self.timer );
                            self.timer = null;

                        }, false);
                        self.el.addEventListener( "mouseout", function() {
                            self.action();

                        }, false);
                    }


                };

                document.addEventListener( "DOMContentLoaded", function() {

                    var slider = new Slideshow( "#main-slider" );

                });


            })();

        </script>
		<script src="../../../resources/js/TweenLite.min.js"></script>
		<script src="../../../resources/js/EasePack.min.js"></script>
		<script src="../../../resources/js/rAF.js"></script>
		<script src="../../../resources/js/demo-1.js"></script>
	</body>
</html>