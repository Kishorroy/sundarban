<?php
/**
 * Created by PhpStorm.
 * User: kishor
 * Date: 10/23/2017
 * Time: 10:10 PM
 */

namespace App\Computer;



use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;
use PDO;
class Computer extends Database
{
public $id, $title, $image,$content,$price, $subCatagory, $catagory;

public function setData($postArray){
    if(array_key_exists("id",$postArray))
        $this->id = $postArray["id"];
    if(array_key_exists("Title",$postArray)){
        $this->title=$postArray['Title'];
    }
    if(array_key_exists("Image",$postArray)){
        $this->image=$postArray['Image'];
    }
    if(array_key_exists("Content",$postArray)){
        $this->content=$postArray['Content'];
    }
    if(array_key_exists("Price",$postArray)){
        $this->price=$postArray['Price'];
    }
    if(array_key_exists("Subcat",$postArray)){
        $this->subCatagory=$postArray['Subcat'];
    }
    if(array_key_exists("Catagory",$postArray)){
        $this->catagory=$postArray['Catagory'];
    }
}
public function store(){
    $sqlQuery = "INSERT INTO products ( title, image, content, price,  subcat, category) VALUES (?,?,?,?,?,?)";
    $sth = $this->conn->prepare( $sqlQuery );


    $dataArray = [ $this->title, $this->image,$this->content,$this->price, $this->subCatagory,$this->catagory ];

    $status = $sth->execute($dataArray);

    if ($status) {
        Message::message("Success!Data has been inserted sucessfully.<br>");
    } else {
        Message::message("Data Has not been Inserted Successfully");
    }
}
    public function index(){

        $sqlQuery = "SELECT * FROM products where category='8'";

        $sth = $this->conn->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;

    }
    public function update(){

        $sqlQuery = "UPDATE products SET title=?, image=?, content=?, price=?, subcat=?, category=? WHERE id=".$this->id;

        $sth = $this->conn->prepare( $sqlQuery );


        $dataArray = [ $this->title, $this->image , $this->content , $this->price, $this->subCatagory,$this->catagory];

        $status = $sth->execute($dataArray);

        if($status)

            Message::message("Success! Data has been updated successfully<br>");
        else
            Message::message("Failed! Data has not been updated<br>");

    }
    public function view(){

        $sqlQuery = "SELECT * FROM products WHERE id=".$this->id;

        $sth = $this->conn->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData=  $sth->fetch();
        return $oneData;

    }
    public function delete(){

        $sqlQuery = "DELETE FROM products where id=".$this->id;

        $status = $this->conn->exec($sqlQuery);


        if($status)

            Message::message("Success! Data has been deleted successfully<br>");
        else
            Message::message("Failed! Data has not been deleted<br>");


    }
    public function deleteMultiple($selectedIDs){

        if( count($selectedIDs) == 0 ){
            Message::message("Empty Selection! Please Select Some Records.<br>");
            return;
        }


        $status = true;

        foreach ($selectedIDs as $id){

            $sqlQuery = "DELETE FROM products  WHERE id=$id";

            if( !  $this->conn->exec($sqlQuery)  )   $status = false;
        }

        if($status)
            Message::message("Success! All Selected Data Has Been Deleted Successfully<br>");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted<br>");



    }
}