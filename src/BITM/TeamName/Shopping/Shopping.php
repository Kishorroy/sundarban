<?php
/**
 * Created by PhpStorm.
 * User: UB
 * Date: 10/14/2017
 * Time: 9:28 AM
 */

namespace App\Shopping;

use App\Model\Database;
use PDO;
use PDOException;

class Shopping extends Database
{
    public $name,$id,$val;

    public function setData($postArray){

        if(array_key_exists("Name",$postArray))
            $this->name=$postArray['Name'];
        if(array_key_exists("id",$postArray))
            $this->id=$postArray['id'];
        if(array_key_exists("value",$postArray))
            $this->val=$postArray['value'];


    }

    public function show(){

        $sql="SELECT * from products INNER JOIN subcat ON subcat.id = products.subcat";

        $sth=$this->conn->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$sth->fetchAll();
        return $alldata;

    }


    public function nav(){

        $sql="SELECT * from category";

        $sth=$this->conn->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $oneData=$sth->fetchAll();
        return $oneData;

    }


    public function nav_drop(){

        $sql="SELECT subcat.title , subcat.category from subcat INNER JOIN category ON category.id=subcat.category ";

        $sth=$this->conn->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData=$sth->fetchAll();
        return $allData;

    }

    public function nav_drop1(){

        $sql="SELECT * from products where products.subcat= ".$this->val;

        $sth=$this->conn->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $catData=$sth->fetchAll();
        return $catData;

    }

    public function details(){

        $sql="SELECT * from products WHERE products.id=".$this->id;
        $sth=$this->conn->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $detData=$sth->fetchAll();
        return $detData;

    }
}