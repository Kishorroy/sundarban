-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2017 at 08:48 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sundarban`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`) VALUES
(8, 'Computer & Laptop'),
(9, 'Mobile & Notepad'),
(10, 'Camera & Lens'),
(11, 'Monitor'),
(12, 'Office Equipment'),
(13, 'Network Accessories'),
(14, 'Computer Accessories'),
(15, 'Mobile Accessories'),
(16, 'Software');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_id` int(100) NOT NULL,
  `order_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `accepted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `lastupdate` date NOT NULL,
  `subcat` int(11) NOT NULL,
  `category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `image`, `content`, `price`, `quantity`, `lastupdate`, `subcat`, `category`) VALUES
(19, 'Samsung Galaxy J2 (2017)', 'samsung_galaxy_j2.jpeg', 'Samsung Galaxy J2 (2017) smartphone was launched in October 2017. The phone comes with a 4.70-inch touchscreen display with a resolution of 540 pixels by 960 pixels. Samsung Galaxy J2 (2017) price in India starts from Rs. 7,390. \r\n\r\nThe Samsung Galaxy J2 (2017) is powered by 1.3GHz quad-core Exynos processor and it comes with 1GB of RAM. The phone packs 8GB of internal storage that can be expanded up to 128GB via a microSD card. As far as the cameras are concerned, the Samsung Galaxy J2 (2017) packs a 5-megapixel primary camera on the rear and a 2-megapixel front shooter for selfies.\r\n\r\nThe Samsung Galaxy J2 (2017) runs Android and is powered by a 2000mAh removable battery. It measures 136.50 x 69.00 x 8.40 (height x width x thickness) and weigh 130.00 grams.\r\n\r\nThe Samsung Galaxy J2 (2017) is a dual SIM (GSM and GSM) smartphone that accepts Micro-SIM and Micro-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, USB OTG, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 137, 10, '2017-10-22', 34, 9),
(20, 'Samsung Galaxy C8', 'galaxy_c8_db.jpg', 'Samsung Galaxy C8 smartphone was launched in September 2017. The phone comes with a 5.50-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels.\r\n\r\nThe Samsung Galaxy C8 is powered by 1.69GHz octa-core processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Samsung Galaxy C8 packs a 13-megapixel primary camera on the rear and a 16-megapixel front shooter for selfies.\r\n\r\nThe Samsung Galaxy C8 runs Android and is powered by a 3000mAh non removable battery.\r\n\r\nThe Samsung Galaxy C8 is a dual SIM (GSM and GSM) . Connectivity options include Wi-Fi, GPS, Bluetooth, 3G and 4G. Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer and Ambient light sensor.', 544, 8, '2017-10-21', 34, 9),
(21, 'Samsung Galaxy J7+', 'galaxyj7_db.jpeg', 'Samsung Galaxy J7+ smartphone was launched in September 2017. The phone comes with a 5.50-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels.\r\n\r\nThe Samsung Galaxy J7+ is powered by 2.4GHz octa-core Helio P20 processor and it comes with 4GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Samsung Galaxy J7+ packs a 13-megapixel primary camera on the rear and a 16-megapixel front shooter for selfies.\r\n\r\nThe Samsung Galaxy J7+ runs Android 7.0 and is powered by a 3000mAh non removable battery. It measures 152.40 x 74.70 x 7.99 (height x width x thickness) .\r\n\r\nThe Samsung Galaxy J7+ is a dual SIM (GSM and GSM) . Connectivity options include Wi-Fi, GPS, Bluetooth, Headphones and 4G.', 545, 8, '2017-10-20', 34, 9),
(22, 'Samsung Galaxy Note 8', 'samsung_galaxy_note_8.jpg', 'Samsung Galaxy Note 8 smartphone was launched in August 2017. The phone comes with a 6.30-inch touchscreen display with a resolution of 1440 pixels by 2960 pixels. Samsung Galaxy Note 8 price in India starts from Rs. 67,900. \r\n\r\nThe Samsung Galaxy Note 8 is powered by 1.7GHz octa-core Samsung Exynos 9 Octa 8895 processor and it comes with 6GB of RAM. The phone packs 64GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Samsung Galaxy Note 8 packs a 12-megapixel primary camera on the rear and a 8-megapixel front shooter for selfies.\r\n\r\nThe Samsung Galaxy Note 8 runs Android 7.1.1 and is powered by a 3300mAh non removable battery. It measures 162.50 x 74.80 x 8.60 (height x width x thickness) and weigh 195.00 grams.\r\n\r\nThe Samsung Galaxy Note 8 is a single SIM (GSM) smartphone that accepts a Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, USB OTG, Headphones, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor, Gyroscope and Barometer.', 900, 10, '2017-10-22', 34, 9),
(23, 'Samsung Galaxy J7 Nxt', 'samsung_galaxy_j7_nxt.jpeg', 'Samsung Galaxy J7 Nxt smartphone was launched in July 2017. The phone comes with a 5.50-inch touchscreen display with a resolution of 720 pixels by 1280 pixels. Samsung Galaxy J7 Nxt price in India starts from Rs. 11,490. \r\n\r\nThe Samsung Galaxy J7 Nxt is powered by 1.6GHz octa-core processor and it comes with 2GB of RAM. The phone packs 16GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Samsung Galaxy J7 Nxt packs a 13-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies.\r\n\r\nThe Samsung Galaxy J7 Nxt runs Android 7.0 and is powered by a 3000mAh non removable battery. It measures 152.40 x 78.60 x 7.60 (height x width x thickness) and weigh 170.00 grams.\r\n\r\nThe Samsung Galaxy J7 Nxt is a dual SIM (GSM and GSM) smartphone that accepts Micro-SIM and Micro-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, Headphones, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Proximity sensor, Accelerometer and Ambient light sensor.', 255, 12, '2017-10-21', 34, 9),
(24, 'Samsung Galaxy J5 Pro', 'samsung_galaxy_j5_pro.jpeg', 'Samsung Galaxy J5 Pro smartphone was launched in June 2017. The phone comes with a 5.20-inch touchscreen display with a resolution of 720 pixels by 1280 pixels.\r\n\r\nThe Samsung Galaxy J5 Pro is powered by 1.6GHz octa-core Exynos 7870 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Samsung Galaxy J5 Pro packs a 13-megapixel primary camera on the rear and a 13-megapixel front shooter for selfies.\r\n\r\nThe Samsung Galaxy J5 Pro runs Android 7.0 and is powered by a 3000mAh non removable battery. It measures 146.30 x 71.30 x 7.90 (height x width x thickness) .\r\n\r\nThe Samsung Galaxy J5 Pro is a dual SIM (GSM and GSM) smartphone that accepts Micro-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, FM, 3G and 4G. Sensors on the phone include Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 480, 10, '2017-10-21', 34, 9),
(25, 'Samsung Galaxy Note Edge', 'samsung_galaxy_note_edge.jpeg', 'Samsung Galaxy Note Edge smartphone was launched in September 2014. The phone comes with a 5.60-inch touchscreen display with a resolution of 1600 pixels by 2560 pixels. Samsung Galaxy Note Edge price in India starts from Rs. 29,950. \r\n\r\nThe Samsung Galaxy Note Edge is powered by 2.7GHz quad-core Qualcomm Snapdragon 805 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 64GB via a microSD card. As far as the cameras are concerned, the Samsung Galaxy Note Edge packs a 16-megapixel primary camera on the rear and a 3.7-megapixel front shooter for selfies.\r\n\r\nThe Samsung Galaxy Note Edge runs Android 4.4 and is powered by a 3000mAh removable battery. It measures 151.30 x 82.40 x 8.30 (height x width x thickness) and weigh 174.00 grams.\r\n\r\nThe Samsung Galaxy Note Edge is a single SIM (GSM) smartphone that accepts a Micro-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, Infrared, 3G and 4G. Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor, Gyroscope and Barometer.', 1442, 8, '2017-10-22', 34, 9),
(26, 'Samsung Galaxy Note 5', 'samsung_galaxy_note_5.jpeg', 'Samsung Galaxy Note 5 smartphone was launched in August 2015. The phone comes with a 5.70-inch touchscreen display with a resolution of 1440 pixels by 2560 pixels at a PPI of 518 pixels per inch. Samsung Galaxy Note 5 price in India starts from Rs. 49,500. \r\n\r\nThe Samsung Galaxy Note 5 is powered by 1.5GHz octa-core Exynos 7420 processor and it comes with 4GB of RAM. The phone packs 32GB of internal storage that cannot be expanded. As far as the cameras are concerned, the Samsung Galaxy Note 5 packs a 16-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies.\r\n\r\nThe Samsung Galaxy Note 5 runs Android 5.1.1 and is powered by a 3000mAh non removable battery. It measures 153.20 x 76.10 x 7.60 (height x width x thickness) and weigh 171.00 grams.\r\n\r\nThe Samsung Galaxy Note 5 is a single SIM (GSM) smartphone that accepts a Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, USB OTG, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor, Gyroscope and Barometer.', 816, 9, '2017-10-21', 34, 9),
(27, 'Xiaomi Redmi Note 4', 'xiaomi_redmi_note_4.jpeg', 'Xiaomi Redmi Note 4 smartphone was launched in August 2016. The phone comes with a 5.50-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels at a PPI of 401 pixels per inch. Xiaomi Redmi Note 4 price in India starts from Rs. 10,999. \r\n\r\nThe Xiaomi Redmi Note 4 is powered by 2GHz octa-core Qualcomm Snapdragon 625 processor and it comes with 4GB of RAM. The phone packs 64GB of internal storage that can be expanded up to 128GB via a microSD card. As far as the cameras are concerned, the Xiaomi Redmi Note 4 packs a 13-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies.\r\n\r\nThe Xiaomi Redmi Note 4 runs Android 6.0 and is powered by a 4100mAh non removable battery. It measures 151.00 x 76.00 x 8.30 (height x width x thickness) and weigh 175.00 grams.\r\n\r\nThe Xiaomi Redmi Note 4 is a dual SIM (GSM and GSM) smartphone that accepts Micro-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, Infrared, USB OTG, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 222, 10, '2017-10-22', 35, 9),
(28, 'Xiaomi Redmi 5A', 'xiaomi_redmi_5a.jpeg', 'Xiaomi Redmi 5A smartphone was launched in October 2017. The phone comes with a 5.00-inch touchscreen display with a resolution of 720 pixels by 1280 pixels at a PPI of 294 pixels per inch. \r\n\r\nThe Xiaomi Redmi 5A is powered by 1.4GHz quad-core Qualcomm Snapdragon 425 processor and it comes with 2GB of RAM. The phone packs 16GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Xiaomi Redmi 5A packs a 13-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies.\r\n\r\nThe Xiaomi Redmi 5A runs Android 7.1 and is powered by a 3000mAh non removable battery. It measures 140.40 x 70.10 x 8.40 (height x width x thickness) and weigh 137.00 grams.\r\n\r\nThe Xiaomi Redmi 5A is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, USB OTG, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Proximity sensor and Accelerometer.', 140, 8, '2017-10-21', 35, 9),
(29, 'Xiaomi Mi Note 3', 'xiaomi_mi_note_3_blue.jpeg', 'Xiaomi Mi Note 3 smartphone was launched in September 2017. The phone comes with a 5.50-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels at a PPI of 403 pixels per inch. \r\n\r\nThe Xiaomi Mi Note 3 is powered by octa-core Qualcomm Snapdragon 660 processor and it comes with 6GB of RAM. The phone packs 64GB of internal storage that cannot be expanded. As far as the cameras are concerned, the Xiaomi Mi Note 3 packs a 12-megapixel primary camera on the rear and a 16-megapixel front shooter for selfies.\r\n\r\nThe Xiaomi Mi Note 3 runs Android and is powered by a 3500mAh non removable battery. It measures 152.60 x 73.95 x 7.60 (height x width x thickness) and weigh 163.00 grams.\r\n\r\nThe Xiaomi Mi Note 3 is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, Infrared, 3G and 4G. Sensors on the phone include Proximity sensor, Accelerometer, Ambient light sensor, Gyroscope and Barometer.', 577, 9, '2017-10-21', 35, 9),
(30, 'Xiaomi Mi MIX 2 Special Edition', 'xiaomi_mi_mix_white.jpeg', 'Xiaomi Mi MIX 2 Special Edition smartphone was launched in September 2017. The phone comes with a 5.99-inch touchscreen display with a resolution of 1080 pixels by 2160 pixels at a PPI of 403 pixels per inch. \r\n\r\nThe Xiaomi Mi MIX 2 Special Edition is powered by octa-core Qualcomm Snapdragon 835 processor and it comes with 8GB of RAM. The phone packs 128GB of internal storage that cannot be expanded. As far as the cameras are concerned, the Xiaomi Mi MIX 2 Special Edition packs a 12-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies.\r\n\r\nThe Xiaomi Mi MIX 2 Special Edition runs Android and is powered by a 3400mAh non removable battery. It measures 150.50 x 74.60 x 7.70 (height x width x thickness) and weigh 187.00 grams.\r\n\r\nThe Xiaomi Mi MIX 2 Special Edition is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, 3G and 4G. Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor, Gyroscope and Barometer.', 950, 8, '2017-10-22', 35, 9),
(31, 'Xiaomi Mi A1', 'xiaomi_mi_a1_black.jpg', 'Xiaomi Mi A1 smartphone was launched in September 2017. The phone comes with a 5.50-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels. Xiaomi Mi A1 price in India starts from Rs. 14,999. \r\n\r\nThe Xiaomi Mi A1 is powered by 2GHz octa-core Qualcomm Snapdragon 625 processor and it comes with 4GB of RAM. The phone packs 64GB of internal storage that can be expanded up to 128GB via a microSD card. As far as the cameras are concerned, the Xiaomi Mi A1 packs a 12-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies.\r\n\r\nThe Xiaomi Mi A1 runs Android 7.1.2 and is powered by a 3080mAh non removable battery. It measures 155.40 x 75.80 x 7.30 (height x width x thickness) and weigh 168.00 grams.\r\n\r\nThe Xiaomi Mi A1 is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, Infrared, USB OTG, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.\r\n', 333, 10, '2017-10-22', 35, 9),
(32, 'Xiaomi Redmi Note 5A', 'xiaomi_redminote5a_db.jpeg', 'Xiaomi Redmi Note 5A smartphone was launched in August 2017. The phone comes with a 5.50-inch touchscreen display with a resolution of 720 pixels by 1280 pixels.\r\n\r\nThe Xiaomi Redmi Note 5A is powered by quad-core Snapdragon 425 processor and it comes with 2GB of RAM. The phone packs 16GB of internal storage that can be expanded up to 128GB via a microSD card. As far as the cameras are concerned, the Xiaomi Redmi Note 5A packs a 13-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies.\r\n\r\nThe Xiaomi Redmi Note 5A runs Android 7.0 and is powered by a 3080mAh non removable battery. It measures 153.00 x 76.20 x 7.50 (height x width x thickness) and weigh 150.00 grams.\r\n\r\nThe Xiaomi Redmi Note 5A is a dual SIM (GSM + CDMA and GSM + CDMA) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth and 4G. Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer and Ambient light sensor.', 500, 8, '2017-10-21', 35, 9),
(33, 'Xiaomi Mi 6', 'xiaomi_mi_6.jpeg', 'Xiaomi Mi 6 smartphone was launched in April 2017. The phone comes with a 5.15-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels at a PPI of 428 pixels per inch. \r\n\r\nThe Xiaomi Mi 6 is powered by 2.45GHz octa-core Qualcomm Snapdragon 835 processor and it comes with 6GB of RAM. The phone packs 64GB of internal storage that cannot be expanded. As far as the cameras are concerned, the Xiaomi Mi 6 packs a 12-megapixel primary camera on the rear and a 8-megapixel front shooter for selfies.\r\n\r\nThe Xiaomi Mi 6 runs Android and is powered by a 3350mAh non removable battery. It measures 145.17 x 70.49 x 7.45 (height x width x thickness) and weigh 182.00 grams.\r\n\r\nThe Xiaomi Mi 6 is a dual SIM (GSM and GSM) smartphone that accepts Micro-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, FM, 3G and 4G. Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 410, 10, '2017-10-21', 35, 9),
(34, 'Xiaomi Mi Note 2', 'xiaomi_mi_note_2_black.jpeg', 'Xiaomi Mi Note 2 smartphone was launched in October 2016. The phone comes with a 5.70-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels.\r\n\r\nThe Xiaomi Mi Note 2 is powered by quad-core Qualcomm Snapdragon 821 processor and it comes with 4GB of RAM. The phone packs 64GB of internal storage that cannot be expanded. As far as the cameras are concerned, the Xiaomi Mi Note 2 packs a 22.56-megapixel primary camera on the rear and a 8-megapixel front shooter for selfies.\r\n\r\nThe Xiaomi Mi Note 2 runs Android and is powered by a 4070mAh non removable battery. It measures 156.20 x 77.30 x 7.60 (height x width x thickness) and weigh 166.00 grams.\r\n\r\nThe Xiaomi Mi Note 2 is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, Infrared, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor, Gyroscope and Barometer.', 622, 9, '2017-10-20', 35, 9),
(35, 'Huawei Mate 10 Lite', 'huawei_mate_10_lite.jpeg', 'Huawei Mate 10 Lite smartphone was launched in October 2017. The phone comes with a 5.90-inch touchscreen display with a resolution of 1080 pixels by 2160 pixels.\r\n\r\nThe Huawei Mate 10 Lite is powered by 1.7GHz octa-core HiSilicon Kirin 659 processor and it comes with 4GB of RAM. The phone packs 64GB of internal storage that can be expanded up to 256GB. As far as the cameras are concerned, the Huawei Mate 10 Lite packs a 16-megapixel primary camera on the rear and a 13-megapixel front shooter for selfies.\r\n\r\nThe Huawei Mate 10 Lite runs Android 7.0 and is powered by a 3340mAh non removable battery. It measures 156.20 x 75.00 x 7.50 (height x width x thickness) and weigh 164.00 grams.\r\n\r\nThe Huawei Mate 10 Lite is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, USB OTG, FM, 3G and 4G. Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 220, 10, '2017-10-21', 37, 9),
(36, 'Huawei Mate 10 Pro', 'huawei_mate_10_pro.jpeg', 'Huawei Mate 10 Pro smartphone was launched in October 2017. The phone comes with a 6.00-inch touchscreen display with a resolution of 1080 pixels by 2160 pixels at a PPI of 402 pixels per inch. \r\n\r\nThe Huawei Mate 10 Pro is powered by 1.8GHz octa-core Huawei HiSilicon Kirin 970 processor and it comes with 4GB of RAM. The phone packs 64GB of internal storage that cannot be expanded. As far as the cameras are concerned, the Huawei Mate 10 Pro packs a 20-megapixel primary camera on the rear and a 8-megapixel front shooter for selfies.\r\n\r\nThe Huawei Mate 10 Pro runs Android 8.0 and is powered by a 4000mAh non removable battery. It measures 154.20 x 74.50 x 7.90 (height x width x thickness) and weigh 178.00 grams.\r\n\r\nThe Huawei Mate 10 Pro is a dual SIM (GSM and GSM) . Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, Infrared, USB OTG, Headphones, 3G and 4G. Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor, Gyroscope and Barometer.', 300, 10, '2017-10-22', 37, 9),
(37, 'Huawei Honor 6C Pro', 'huawei_honor_6c_pro.jpeg', 'Huawei Honor 6C Pro smartphone was launched in October 2017. The phone comes with a 5.20-inch touchscreen display with a resolution of 720 pixels by 1280 pixels.\r\n\r\nThe Huawei Honor 6C Pro is powered by 1.5GHz octa-core Mediatek MT6750 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 128GB via a microSD card. As far as the cameras are concerned, the Huawei Honor 6C Pro packs a 13-megapixel primary camera on the rear and a 8-megapixel front shooter for selfies.\r\n\r\nThe Huawei Honor 6C Pro runs Android 7.0 and is powered by a 3000mAh non removable battery. It measures 147.90 x 73.20 x 7.65 (height x width x thickness) and weigh 145.00 grams.\r\n\r\nThe Huawei Honor 6C Pro is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, USB OTG, 3G and 4G. Sensors on the phone include Proximity sensor, Accelerometer and Ambient light sensor.', 400, 8, '2017-10-20', 37, 9),
(38, 'Huawei Honor 7X', 'huawei_honor_7x.jpeg', 'Huawei Honor 7X smartphone was launched in October 2017. The phone comes with a 5.93-inch touchscreen display with a resolution of 1080 pixels by 2160 pixels.\r\n\r\nThe Huawei Honor 7X is powered by 1.7GHz octa-core HiSilicon Kirin 659 processor and it comes with 4GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Huawei Honor 7X packs a 16-megapixel primary camera on the rear and a 8-megapixel front shooter for selfies.\r\n\r\nThe Huawei Honor 7X runs Android 7.0 and is powered by a 3340mAh non removable battery. It measures 156.50 x 75.30 x 7.60 (height x width x thickness) and weigh 165.00 grams.\r\n\r\nThe Huawei Honor 7X is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, USB OTG, 3G and 4G. Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 470, 9, '2017-10-21', 37, 9),
(39, 'Huawei Y6 (2017)', 'huawei_y6.jpeg', 'Huawei Y6 (2017) smartphone was launched in May 2017. The phone comes with a 5.00-inch touchscreen display with a resolution of 720 pixels by 1280 pixels.\r\n\r\nThe Huawei Y6 (2017) is powered by 1.4GHz quad-core MediaTek MT6737T processor and it comes with 2GB of RAM. The phone packs 16GB of internal storage that can be expanded. As far as the cameras are concerned, the Huawei Y6 (2017) packs a 13-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies.\r\n\r\nThe Huawei Y6 (2017) runs Android 6.0 and is powered by a 3000mAh non removable battery. It measures 143.80 x 72.00 x 8.50 (height x width x thickness) and weigh 150.00 grams.\r\n\r\nThe Huawei Y6 (2017) is a dual SIM (GSM and GSM) . Connectivity options include Wi-Fi, GPS, Bluetooth, 3G and 4G. Sensors on the phone include Proximity sensor, Accelerometer and Ambient light sensor.', 400, 9, '2017-10-22', 37, 9),
(40, 'Huawei Y7 Prime', 'huawei_y7prime_db.jpeg', 'Huawei Y7 Prime smartphone was launched in June 2017. The phone comes with a 5.50-inch touchscreen display with a resolution of 720 pixels by 1290 pixels.\r\n\r\nThe Huawei Y7 Prime is powered by 1.4GHz octa-core Qualcomm Snapdragon 435 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Huawei Y7 Prime packs a 12-megapixel primary camera on the rear and a 8-megapixel front shooter for selfies.\r\n\r\nThe Huawei Y7 Prime runs Android 7.0 and is powered by a 4000mAh non removable battery.\r\n\r\nThe Huawei Y7 Prime is a dual SIM (GSM + CDMA and GSM + CDMA) . Connectivity options include Wi-Fi, GPS, Bluetooth, USB OTG, FM, 3G and 4G. Sensors on the phone include Proximity sensor, Accelerometer and Ambient light sensor.', 440, 10, '2017-10-21', 37, 9),
(41, 'Lenovo K6 Note', 'lenovo_k6_note.jpg', 'Lenovo K6 Note smartphone was launched in September 2016. The phone comes with a 5.50-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels at a PPI of 401 pixels per inch. Lenovo K6 Note price in India starts from Rs. 10,290. \r\n\r\nThe Lenovo K6 Note is powered by 1.4GHz octa-core Qualcomm Snapdragon 430 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 128GB via a microSD card. As far as the cameras are concerned, the Lenovo K6 Note packs a 16-megapixel primary camera on the rear and a 8-megapixel front shooter for selfies.\r\n\r\nThe Lenovo K6 Note runs Android 6.0.1 and is powered by a 4000mAh non removable battery. It measures 151.00 x 76.00 x 8.40 (height x width x thickness) and weigh 169.00 grams.\r\n\r\nThe Lenovo K6 Note is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, USB OTG, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 311, 7, '2017-10-22', 39, 9),
(42, 'Lenovo K8', 'lenovo_k8.jpeg', 'Lenovo K8 smartphone was launched in September 2017. The phone comes with a 5.20-inch touchscreen display with a resolution of 720 pixels by 1280 pixels. Lenovo K8 price in India starts from Rs. 10,099. \r\n\r\nThe Lenovo K8 is powered by 2.3GHz octa-core MediaTek Helio P20 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 128GB via a microSD card. As far as the cameras are concerned, the Lenovo K8 packs a 13-megapixel primary camera on the rear and a 8-megapixel front shooter for selfies.\r\n\r\nThe Lenovo K8 runs Android 7.1.1 and is powered by a 4000mAh non removable battery. It measures 147.90 x 73.70 x 8.55 (height x width x thickness) and weigh 165.00 grams.\r\n\r\nThe Lenovo K8 is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, USB OTG, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 288, 9, '2017-10-20', 39, 9),
(43, 'Lenovo K8 Note', 'lenovo_k8_note.jpg', 'Lenovo K8 Note smartphone was launched in August 2017. The phone comes with a 5.50-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels at a PPI of 441 pixels per inch. Lenovo K8 Note price in India starts from Rs. 12,999. \r\n\r\nThe Lenovo K8 Note is powered by 1.4GHz MediaTek Helio X23 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 128GB via a microSD card. As far as the cameras are concerned, the Lenovo K8 Note packs a 13-megapixel primary camera on the rear and a 13-megapixel front shooter for selfies.\r\n\r\nThe Lenovo K8 Note runs Android 7.1.1 and is powered by a 4000mAh non removable battery. It measures 154.50 x 75.90 x 8.50 (height x width x thickness) and weigh 180.00 grams.\r\n\r\nThe Lenovo K8 Note is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, USB OTG, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 288, 10, '2017-10-21', 39, 9),
(44, 'Lenovo P2', 'lenovo_p2.jpeg', 'Lenovo P2 smartphone was launched in September 2016. The phone comes with a 5.50-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels at a PPI of 401 pixels per inch. Lenovo P2 price in India starts from Rs. 13,499. \r\n\r\nThe Lenovo P2 is powered by 2GHz octa-core Qualcomm Snapdragon 625 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 128GB via a microSD card. As far as the cameras are concerned, the Lenovo P2 packs a 13-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies.\r\n\r\nThe Lenovo P2 runs Android 6.0 and is powered by a 5100mAh non removable battery. It measures 153.00 x 76.00 x 8.20 (height x width x thickness) and weigh 177.00 grams.\r\n\r\nThe Lenovo P2 is a dual SIM (GSM and GSM) smartphone that accepts Micro-SIM and Regular. Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, USB OTG, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 377, 8, '2017-10-22', 39, 9),
(45, 'Lenovo Vibe K4 Note', 'lenovo_vibe_k4_note.jpeg', 'Lenovo Vibe K4 Note smartphone was launched in January 2016. The phone comes with a 5.50-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels at a PPI of 401 pixels per inch. Lenovo Vibe K4 Note price in India starts from Rs. 10,400. \r\n\r\nThe Lenovo Vibe K4 Note is powered by 1.3GHz octa-core MediaTek MT6753 processor and it comes with 3GB of RAM. The phone packs 16GB of internal storage that can be expanded up to 128GB via a microSD card. As far as the cameras are concerned, the Lenovo Vibe K4 Note packs a 13-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies.\r\n\r\nThe Lenovo Vibe K4 Note runs Android 5.1 and is powered by a 3300mAh non removable battery. It measures 153.60 x 76.50 x 9.20 (height x width x thickness) and weigh 158.00 grams.\r\n\r\nThe Lenovo Vibe K4 Note is a dual SIM (GSM and GSM) smartphone that accepts Micro-SIM and Micro-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, USB OTG, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 266, 8, '2017-10-20', 39, 9),
(46, 'Lenovo Vibe K5 Note', 'lenovo_vibe_k5_note.jpeg', 'Lenovo Vibe K5 Note smartphone was launched in January 2016. The phone comes with a 5.50-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels at a PPI of 401 pixels per inch. Lenovo Vibe K5 Note price in India starts from Rs. 10,499. \r\n\r\nThe Lenovo Vibe K5 Note is powered by 1.8GHz octa-core MediaTek Helio P10 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 128GB via a microSD card. As far as the cameras are concerned, the Lenovo Vibe K5 Note packs a 13-megapixel primary camera on the rear and a 8-megapixel front shooter for selfies.\r\n\r\nThe Lenovo Vibe K5 Note runs Android 6.0 and is powered by a 3500mAh non removable battery. It measures 152.00 x 75.70 x 8.40 (height x width x thickness) and weigh 165.00 grams.\r\n\r\nThe Lenovo Vibe K5 Note is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, NFC, USB OTG, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 300, 8, '2017-10-21', 39, 9),
(47, 'Oppo A57', 'oppo_a57_gold.jpeg', 'Oppo A57 smartphone was launched in November 2016. The phone comes with a 5.20-inch touchscreen display with a resolution of 720 pixels by 1280 pixels. Oppo A57 price in India starts from Rs. 12,499. \r\n\r\nThe Oppo A57 is powered by 1.4GHz octa-core Qualcomm Snapdragon 435 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Oppo A57 packs a 13-megapixel primary camera on the rear and a 16-megapixel front shooter for selfies.\r\n\r\nThe Oppo A57 runs Android 6.0 and is powered by a 2900mAh non removable battery. It measures 149.10 x 72.90 x 7.65 (height x width x thickness) and weigh 147.00 grams.\r\n\r\nThe Oppo A57 is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, USB OTG, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 377, 8, '2017-10-21', 36, 9),
(48, 'Oppo A71', 'oppo_a71.jpeg', 'Oppo A71 smartphone was launched in September 2017. The phone comes with a 5.20-inch touchscreen display with a resolution of 720 pixels by 1280 pixels. Oppo A71 price in India starts from Rs. 11,500. \r\n\r\nThe Oppo A71 is powered by 1.5GHz octa-core MediaTek MT6750 processor and it comes with 3GB of RAM. The phone packs 16GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Oppo A71 packs a 13-megapixel primary camera on the rear and a 5-megapixel front shooter for selfies.\r\n\r\nThe Oppo A71 runs Android 7.1 and is powered by a 3000mAh non removable battery. It measures 148.10 x 73.80 x 7.60 (height x width x thickness) and weigh 137.00 grams.\r\n\r\nThe Oppo A71 is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, USB OTG, 3G and 4G. Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer and Ambient light sensor.', 288, 9, '2017-10-22', 36, 9),
(49, 'Oppo F3 Lite', 'oppo_f3_lite.jpeg', 'Oppo F3 Lite smartphone was launched in October 2017. The phone comes with a 5.20-inch touchscreen display with a resolution of 720 pixels by 1280 pixels.\r\n\r\nThe Oppo F3 Lite is powered by 1.4GHz octa-core Qualcomm Snapdragon 435 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Oppo F3 Lite packs a 13-megapixel primary camera on the rear and a 16-megapixel front shooter for selfies.\r\n\r\nThe Oppo F3 Lite runs Android 6.0 and is powered by a 2900mAh non removable battery. It measures 149.10 x 72.90 x 7.65 (height x width x thickness) and weigh 147.00 grams.\r\n\r\nThe Oppo F3 Lite is a dual SIM (GSM and GSM) smartphone that accepts Nano-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, USB OTG, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Compass Magnetometer, Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 300, 10, '2017-10-21', 36, 9),
(50, 'Oppo R11 Plus', 'oppo_r11_plus.jpeg', 'Oppo R11 Plus smartphone was launched in June 2017. The phone comes with a 6.00-inch touchscreen display with a resolution of 1080 pixels by 1920 pixels.\r\n\r\nThe Oppo R11 Plus is powered by octa-core Qualcomm Snapdragon 660 processor and it comes with 4GB of RAM. The phone packs 64GB of internal storage that can be expanded. As far as the cameras are concerned, the Oppo R11 Plus packs a 20-megapixel primary camera on the rear and a 20-megapixel front shooter for selfies.\r\n\r\nThe Oppo R11 Plus runs Android 7.1 and is powered by a 4000mAh non removable battery.\r\n\r\nThe Oppo R11 Plus is a dual SIM (GSM and GSM) smartphone that accepts Micro-SIM and Nano-SIM. Connectivity options include Wi-Fi, GPS, Bluetooth, FM, 3G and 4G (with support for Band 40 used by some LTE networks in India). Sensors on the phone include Proximity sensor, Accelerometer, Ambient light sensor and Gyroscope.', 280, 9, '2017-10-22', 36, 9);

-- --------------------------------------------------------

--
-- Table structure for table `subcat`
--

CREATE TABLE `subcat` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `title` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subcat`
--

INSERT INTO `subcat` (`id`, `category`, `title`) VALUES
(24, 8, 'Mac'),
(25, 8, 'HP'),
(26, 8, 'DELL'),
(27, 8, 'SONY'),
(28, 8, 'LENOVO'),
(29, 8, 'sAMSUNG'),
(30, 8, 'ASUS'),
(31, 8, 'TOSHIBA'),
(32, 8, 'ACER'),
(33, 9, 'IPHONE'),
(34, 9, 'SAMSUNG'),
(35, 9, 'XIAOMI'),
(36, 9, 'OPPO'),
(37, 9, 'HUAWEI'),
(38, 9, 'WALTON'),
(39, 9, 'LENOVO'),
(40, 10, 'NIKON CAMERA'),
(41, 10, 'CANON CAMERA'),
(42, 10, 'NIKON CAMERA BODY'),
(43, 10, 'CANON CAMERA BODY'),
(44, 11, 'SONY'),
(45, 11, 'LG'),
(46, 11, 'SAMSUNG'),
(47, 11, 'HP'),
(48, 12, 'PROJECTOR'),
(49, 12, 'IP CAMERA'),
(50, 12, 'CC CAMERA'),
(51, 12, 'PROJECTOR SCREEN'),
(52, 12, 'PHOTOCOPIER'),
(53, 12, 'SCANNER'),
(54, 12, 'PRINTER'),
(55, 12, 'BARCODE SCANNER'),
(56, 13, 'ROUTER'),
(57, 13, 'MODEM'),
(58, 13, 'UTP CABLE'),
(59, 13, 'SWITCH'),
(60, 13, 'HUB'),
(61, 13, 'LAN CARD'),
(62, 14, 'DESKTOP RAM'),
(63, 14, 'LAPTOP RAM'),
(64, 14, 'KEYBOARD'),
(65, 14, 'GRAPHICS CARD'),
(66, 14, 'MOUSE'),
(67, 14, 'MAIN BOARD'),
(68, 14, 'OPTICAL DEVICE'),
(69, 14, 'EXTERNAL HDD'),
(70, 14, 'SSD'),
(71, 14, 'PENDRIVE'),
(72, 15, 'EARPHONE'),
(73, 15, 'HEADPHONE'),
(74, 15, 'CHARGER'),
(75, 15, 'SCREEN PROTECTOR'),
(76, 16, 'ANTIVIRUS'),
(77, 16, 'OPERATING SYSTEM'),
(78, 16, 'OFFICE SOFTWARE'),
(79, 10, 'SONY'),
(81, 16, 'PHOTOSHOP SOFTWARE');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL,
  `user_type` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`, `user_type`) VALUES
(32, 'Tushar', 'Chowdhury', 'tushar.chowdhury@gmail.com', '71f55003c9a36b40c4a094908f11fb77', '1327487282', 'gfddhfdty', 'Yes', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcat`
--
ALTER TABLE `subcat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `subcat`
--
ALTER TABLE `subcat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
