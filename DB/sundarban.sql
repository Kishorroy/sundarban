-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 22, 2017 at 07:35 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sundarban`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`) VALUES
(8, 'Computer & Laptop'),
(9, 'Mobile & Notepad'),
(10, 'Camera & Lens'),
(11, 'Monitor'),
(12, 'Office Equipment'),
(13, 'Network Accessories'),
(14, 'Computer Accessories'),
(15, 'Mobile Accessories'),
(16, 'Software');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_id` int(100) NOT NULL,
  `order_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `accepted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` int(10) NOT NULL,
  `feature` int(11) NOT NULL DEFAULT '0',
  `quantity` int(10) NOT NULL,
  `lastupdate` date NOT NULL,
  `subcat` int(11) NOT NULL,
  `category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subcat`
--

CREATE TABLE `subcat` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `title` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subcat`
--

INSERT INTO `subcat` (`id`, `category`, `title`) VALUES
(24, 8, 'Mac'),
(25, 8, 'HP'),
(26, 8, 'DELL'),
(27, 8, 'SONY'),
(28, 8, 'LENOVO'),
(29, 8, 'sAMSUNG'),
(30, 8, 'ASUS'),
(31, 8, 'TOSHIBA'),
(32, 8, 'ACER'),
(33, 9, 'IPHONE'),
(34, 9, 'sAMSUNG'),
(35, 9, 'XIAOMI'),
(36, 9, 'OPPO'),
(37, 9, 'VIVO'),
(38, 9, 'WALTON'),
(39, 9, 'SYMPHONY'),
(40, 10, 'NIKON CAMERA'),
(41, 10, 'CANON CAMERA'),
(42, 10, 'NIKON CAMERA BODY'),
(43, 10, 'CANON CAMERA BODY'),
(44, 11, 'SONY'),
(45, 11, 'LG'),
(46, 11, 'SAMSUNG'),
(47, 11, 'HP'),
(48, 12, 'PROJECTOR'),
(49, 12, 'IP CAMERA'),
(50, 12, 'CC CAMERA'),
(51, 12, 'PROJECTOR SCREEN'),
(52, 12, 'PHOTOCOPIER'),
(53, 12, 'SCANNER'),
(54, 12, 'PRINTER'),
(55, 12, 'BARCODE SCANNER'),
(56, 13, 'ROUTER'),
(57, 13, 'MODEM'),
(58, 13, 'UTP CABLE'),
(59, 13, 'SWITCH'),
(60, 13, 'HUB'),
(61, 13, 'LAN CARD'),
(62, 14, 'DESKTOP RAM'),
(63, 14, 'LAPTOP RAM'),
(64, 14, 'KEYBOARD'),
(65, 14, 'GRAPHICS CARD'),
(66, 14, 'MOUSE'),
(67, 14, 'MAIN BOARD'),
(68, 14, 'OPTICAL DEVICE'),
(69, 14, 'EXTERNAL HDD'),
(70, 14, 'SSD'),
(71, 14, 'PENDRIVE'),
(72, 15, 'EARPHONE'),
(73, 15, 'HEADPHONE'),
(74, 15, 'CHARGER'),
(75, 15, 'SCREEN PROTECTOR'),
(76, 16, 'ANTIVIRUS'),
(77, 16, 'OPERATING SYSTEM'),
(78, 16, 'OFFICE SOFTWARE');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL,
  `user_type` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`, `user_type`) VALUES
(32, 'Tushar', 'Chowdhury', 'tushar.chowdhury@gmail.com', '71f55003c9a36b40c4a094908f11fb77', '1327487282', 'gfddhfdty', 'Yes', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcat`
--
ALTER TABLE `subcat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `subcat`
--
ALTER TABLE `subcat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
